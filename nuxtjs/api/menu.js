const Menu = [
  {header: 'Apps'},
  {
    title: 'Timesheet Working',
    group: 'apps',
    icon: 'dashboard',
    name: 'Dashboard',
    href: '/dashboard'
  },
  {
    title: 'Leave Request',
    group: 'apps',
    icon: 'dashboard',
    name: 'Leave Request',
    href: '/leave-request'
  },
  {
    title: 'Static',
    group: 'apps',
    icon: 'dashboard',
    name: 'Static',
    href: '/static'
  },
  {
    title: 'List User',
    group: 'apps',
    icon: 'dashboard',
    name: 'List User',
    href: '/user'
  },
  {
    title: 'History Timesheet',
    group: 'forms',
    component: 'forms',
    icon: 'edit',
    items: [
      {name: 'Person', title: 'Person', href: '/forms/selection-controls'},
      {name: 'People', title: 'People', href: '/forms/selection-controls'},
    ]
  },
  {divider: true},
];
// reorder menu
Menu.forEach((item) => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
  }
});

export default Menu;
